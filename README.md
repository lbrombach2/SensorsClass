The sensors class is intended to glue my various sensors together and be ready to drop into programs on a Raspberry Pi. (Specifically, my current autonomous robot project. More at lloydbrombach.wordpress.com
So far, it has functions for the LSM303 magnetometer(compass) and accelerometer, and the LIDAR from the XV11 Neato Botvac. The roombaSerial code is just extra stuff (and read() is extra crappy from an early, buggy version) for my main() to work with for testing. main() is just a crappy shell interface for testing, and sensors.pp and sensors.h should be able to just be dropped in and included as long as you have pigpio installed. A global object sensors::mySensors is created at the top of sensors.cpp so it can be accessed across many files in my project.

Summary of functions:

	int I2C_Accel()
		//returns 1 if Z axis close to 1 G (device =level-ish). Average of 3 readings. 			Could easily be changes to return a Z (or other axis) value in milliGs.

	void I2C_Comp_Init(); //call once in main()
		reads and restores last calibration and zero() data from a file "lastCal.txt"
		Prompts to calibrate if no file exists or if it's been over 4 hours.
		**I should probably make into a bool and return if calibration good.


	int I2C_Comp_Read();
		Returns cartesian heading per current calibration and zero() offset data 

	int polarHeading();
		calls I2C_Comp_Read() and converts to polar degree heading

	int I2C_Comp_Calibrate();
		Reads 2500 samples to find gauss min and max values for all 3 axis. Make sure 			compass is pretty level and rotated at a full circle over this time.

	bool I2C_Comp_Zero();// adjusts heading to make current direction = 0. Makesure 		calibrated first. Implementation example: Currently runs every time robot undocks 			so robot thinks facing its dock from the "home" position is north
		This offset value gets written to the file "lastCal.txt"

    	int I2C_Comp_Start();
		opens I2C compass handle, sets frequency and mode

	int I2C_Comp_Stop();
		sets compass to sleep, closes handle

	///////////////////for lidar//////////////////////////////////////////

        int scan();
		Spools up lidar and grabs raw data for processing
		this function calls others...(legit() and full()).
		runs for either x number cycles (cycle=a packet grab) or until 
		full() returns that we've acquired > x percentage of the 360 points

        int getDis(int bearing); 
		Returns data from last scan(). Don't forget 0 is at my right and 90 is straight 		ahead (like mathematics unit circle). Returns dataArr[bearing][0]
		Reads it's data from dataArr[360][4] 
		where [0]-[359] are degrees and [0][1][2] are distances [3]=signal strength of [0]


    protected:

    private:
/////////////////for compass and accelerometer (LSM303)//////////////////////


///////////////////for lidar//////////////////////////////////////////

	bool legit(int packet[]); //tests checksum
		verifies that packet sent by scan() is legit. Files packet into dataArr[][] if so

	bool full(); 
		returns true if > a defined percentage of 360 points have been acquired. 
		usually called by lidar_Read()
