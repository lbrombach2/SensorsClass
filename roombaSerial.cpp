//#include <iostream>
//#include <stdio.h>
//#include <cstdio>
//#include <cstdlib>
//#include <string.h>
//#include <errno.h>
//#include <pigpio.h>


#include <iostream>
#include <cerrno>
#include <cstring>
#include <pigpio.h>
#include "roombaSerial.h"
#include "sensors.h"



using namespace std;


int USBHandle = 0;
int controlVoltage = 16000;
int ircodeLeft = 0;
int ircodeRight = 0;

int* read()
{
    const int delayTimeForAvail = 10000; // 0.01s wait time between checking if data has become available
    int dataCount = 0;
    //cout << "Start read" << endl;
    int BSpacket = 1;
    int packet1[65] = {0};
    int bit = 0;
    bool gotPacket, gotValidPacket = false;

    if (USBHandle < 0)
    {//*****IF PIGPIO WONT INTIALIZE, DID YOUR DUMB ASS FORGET TO USE SUDO?******
        cerr << "Unable to open serial device: " << strerror (errno) << endl;
        return NULL;
    }

    while (!gotValidPacket)
    {
        gotPacket = false;
        do
        {
            while ((dataCount = serDataAvailable(USBHandle)) == 0)
                gpioSleep(PI_TIME_RELATIVE, 0, delayTimeForAvail); // Wait for some data to be available
            if (dataCount == PI_BAD_HANDLE)
            {
                cerr << "Something went wrong with the serial handle" << endl;
                return NULL;
            }
          //  cout << "gonna read a byte, USB Handle = " << USBHandle << endl;
            packet1[0] = serReadByte(USBHandle);
            if (packet1[0] < 0)
            {
                cerr << "Something went wrong reading header" << endl;

                return NULL;
            }
            if (packet1[0] == 19)
            {
            //    cout << "Got start bit" << endl;
                while (serDataAvailable(USBHandle) < 2)
                    gpioSleep(PI_TIME_RELATIVE, 0, delayTimeForAvail); // Wait for the data to be available
                packet1[1] = serReadByte(USBHandle);
                packet1[2] = serReadByte(USBHandle);
                if ((packet1[1] < 0) || (packet1[2] < 0))
                {
                    cerr << "Something went wrong reading packet header" << endl;

                    return NULL;
                }
                if (packet1[1] == 60 && packet1[2] == 1) //if these two bytes match, then read another 57 bytes
                {
                    for (bit = 3; bit <= 59; bit++  )
                    {
                        while (serDataAvailable(USBHandle) < 1)
                            gpioSleep(PI_TIME_RELATIVE, 0, delayTimeForAvail); // Wait for some data to be available
                        packet1[bit] = serReadByte(USBHandle);
                        if (packet1[bit] < 0)
                        {   // Something went wrong, we were told we had data to read but the read failed - bail out
                            cerr << "Something went wrong reading packet data" << endl;

                            return NULL;
                        }
                    }
                    gotPacket = true;
                }
            }
        } while (!gotPacket);

        if (packet1[13] == 3 && packet1[24] == 26 && packet1[56] == 26 && packet1[59]==26) //these bytes are always the same in a good packet
        {
            gotValidPacket = true;
        }
        else
        {
            BSpacket++;
            cout <<" bad packet " << BSpacket << endl;
            gpioSleep(PI_TIME_RELATIVE,0,5000);
        }
    }
    return parse(packet1);
}



int *parse(int packet[])
{



int bumpANDwheeldrops = packet[0x03];
int wall = packet[0x04];
int cliffLEft = packet[0x05];
int cliffFrontLEft = packet[0x06];
int cliffFrontRight = packet[0x07];
int cliffRight = packet[0x08];
int virtualWall = packet[0x09];
int overcurrents = packet[0x0a];
int dirtDetect = packet[0x0b];
int chargingState = packet[0x0e];
int voltage = packet[0x10]|(packet[0x0f]<<8);
int current = packet[0x12]|(packet[0x11]<<8);
int batteryTemp = packet[0x13];
int batteryCharge = packet[0x15]|(packet[0x14]<<8);
int batteryCapacity = packet[0x17]|(packet[0x16]<<8);
int encoderCountsLeft = packet[0x1d]|(packet[0x1c]<<8);
int encoderCountsRight= packet[0x1f]|(packet[0x1e]<<8);
int lightBumper = packet[0x20];
int lightBumpLeft = packet[0x22]|(packet[0x21]<<8);
int lightBumpFrontLeft = packet[0x24]|(packet[0x23]<<8);
int lightBumpCenterLeft = packet[0x26]|(packet[0x25]<<8);
int lightBumpCenterRight = packet[0x28]|(packet[0x27]<<8);
int lightBumpFrontRight = packet[0x2a]|(packet[0x29]<<8);
int lightBumpRight = packet[0x2c]|(packet[0x2b]<<8);
int irOpcodeLeft = packet[0x2d];
int irOpcodeRight = packet[0x2e];
int leftMotorCurrent = packet[0x30]|(packet[0x2f]<<8);
int rightMotorCurrent = packet[0x32]|(packet[0x31]<<8);
int mainBrushCurrent = packet[0x34]|(packet[0x33]<<8);
int sideBrushCurrent = packet[0x36]|(packet[0x35]<<8);
int stasis = packet[0x37];
//int distance = packet[0x39]|(packet[0x3a]<<8);  //onboard odometer useless
//int angle = packet[0x3d]|(packet[0x3c]<<8);     //onboard angle monitor useless


static int parsedData[50] = {0};
//cout<< "inparse/////";
parsedData[0] = bumpANDwheeldrops;
parsedData[1] = wall;
parsedData[2] = cliffLEft;
parsedData[3] = cliffFrontLEft;
parsedData[4] = cliffFrontRight;
parsedData[5] = cliffRight;
parsedData[6] = virtualWall;
parsedData[7] = overcurrents;
parsedData[8] = dirtDetect;
parsedData[9] = chargingState;
parsedData[10] = voltage;
parsedData[11] = current;
parsedData[12] = batteryTemp;
parsedData[13] = batteryCharge;
parsedData[14] = batteryCapacity;
parsedData[15] = encoderCountsLeft ;
parsedData[16] = encoderCountsRight;
parsedData[17] = lightBumper;
parsedData[18] = lightBumpLeft;
parsedData[19] = lightBumpFrontLeft;
parsedData[20] = lightBumpCenterLeft;
parsedData[21] = lightBumpCenterRight;
parsedData[22] = lightBumpFrontRight;
parsedData[23] = lightBumpRight ;
parsedData[24] = irOpcodeLeft;
parsedData[25] = irOpcodeRight;
parsedData[26] = leftMotorCurrent ;
parsedData[27] = rightMotorCurrent ;
parsedData[28] = mainBrushCurrent;
parsedData[29] = sideBrushCurrent ;
parsedData[30] = stasis;
//parsedData[31] = distance ;
//parsedData[32] = angle ;

//cout << endl << "distance in parse = " <<distance << endl;
//cout << "bumpsANDwheeldrops = " << bumpANDwheeldrops << endl;
//cout << "voltage = " << voltage << endl;
controlVoltage = voltage;
ircodeLeft = irOpcodeLeft;
ircodeRight = irOpcodeRight;
//cout << "chargng state = " << chargingState << endl;
//cout << "batteryCapacity = " << batteryCapacity << endl;
//cout << "just before int*p WTF is going on//////" ;
int* p = parsedData;
//cout<< "endparse" <<endl<<endl;
return p;

}




void roombaWake() //clicks relay to wake and starts data streaming
{
    if(gpioInitialise() < 0){
	cout << "GPIO initializing failed. Try again asshole." << endl;
	gpioSleep(PI_TIME_RELATIVE,0,1500000);
	}
    USBHandle = serOpen("/dev/ttyUSB0",115200,0);
    //USBHandle = FD;
   // cout << "USBHandle in wake() = " <<USBHandle << endl; gpioSleep(PI_TIME_RELATIVE,5,110000);
    int R1 = 23;
    gpioSetMode(23, PI_OUTPUT);
    gpioWrite(R1, 0);
    gpioSleep(PI_TIME_RELATIVE,0,110000); //should be 50 milliseconds
    gpioWrite(R1, 1); //this is NOT the LED, stupid. Leave it on when not pulsing to wake

    cout << "Waking" << endl;

    //
    serWriteByte(USBHandle,128); //start command/passive mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);
    serWriteByte(USBHandle,131); //safe mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);

    //char startDataString[] = {148,3,3,26,26};
    char startDataString[] = {148,6,1,3,26,101,26,26};
    serWrite(USBHandle,startDataString,8);


    gpioSleep(PI_TIME_RELATIVE,0,150000);
      cout << "Should be awake and streaming" << endl;

}
void undock()
{
    cout << "undocking" <<endl;
    gpioSleep(PI_TIME_RELATIVE,0,150000);
    poke();
    serWriteByte(USBHandle,132); //full mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);
    driveSlowRev();
    gpioSleep(PI_TIME_RELATIVE,3,500000    );
    serWriteByte(USBHandle,131); //safe mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);
    stop();
  //  drive(0,40,00,001);//pivot left nice and slow
  //  gpioSleep(PI_TIME_RELATIVE,5,100000    );
  //  stop();
    }
void dock()
{
    cout << "Looking for dock" <<endl;
    poke();
  //  serWriteByte(USBHandle,143);
    drive(0,40,255,255);//pivot right nice and slow

    for(int i=0;i!=2;){
    read();
    i=0;
    if(ircodeLeft>160&&ircodeLeft<174){i++;}
    if(ircodeRight>160&&ircodeRight<174){i++;}
    }
stop();
    cout << "Dock spotted. Trying to Dock for 15 seconds" <<endl;
    serWriteByte(USBHandle,143); //dock
    gpioSleep(PI_TIME_RELATIVE,15,0);
}
void stop()
{
   // cout<< "Hopefully stopping for 5 seconds" << endl;
    drive(0,0,0,0);
  //  serWrite(USBHandle,stopString,5);
    gpioSleep(PI_TIME_RELATIVE,1,0);
    serWriteByte(USBHandle,132); //back full mode while sitting idle to prevent sleeping

}
void faceDock()
{
    poke();
    drive(0,40,255,255);//pivot right nice and slow
    gpioSleep(PI_TIME_RELATIVE,3,0);

    for(int i=0;i<4;){
    read();
    i=0;
    if(ircodeLeft>160&&ircodeLeft<174){i++;}//cout<<"Left sees it"<<endl;}
    if(ircodeRight>160&&ircodeRight<174){i++;}//cout<<"right see it"<<endl;}
    gpioSleep(PI_TIME_RELATIVE,0,20000);
 //   cout<<ircodeLeft<<"  "<<ircodeRight<<endl;
    read();
    if(ircodeLeft>160&&ircodeLeft<174){i++;}//cout<<"Left sees it"<<endl;}
    if(ircodeRight>160&&ircodeRight<174){i++;}//cout<<"right see it"<<endl;}
    gpioSleep(PI_TIME_RELATIVE,0,20000);
 //   cout<<ircodeLeft<<"   "<<ircodeRight<<endl;
    }

    stop();

}
//////////////////////START FACEHEADING()///////////////////
void faceHeading(){
mySensors.I2C_Comp_Start();
int desired=-1;
do{
    cout<<endl<<"Please enter desired POLAR heading 0-359"<<endl;
    cin>>desired;
}while(desired<0||desired>359);

    poke();
    int refTime=time(NULL);
  //drive(0,40,00,001);//pivot left nice and slow
    drive(0,20,255,255);//pivot right nice and slow
    gpioSleep(PI_TIME_RELATIVE,0,10000);

    while(std::abs(desired-mySensors.polarHeading())>2&&(time(NULL)-refTime)<30){
    gpioSleep(PI_TIME_RELATIVE,0,5000);
    }
    stop();
    gpioSleep(PI_TIME_RELATIVE,1,0);
    cout<<"New Heading After Stop = "<<mySensors.polarHeading()<<endl;
    mySensors.I2C_Comp_Stop();
}
/////////////////////START PIVOT()////////////////////////
void pivot(){
int desired=-1;
do{
    cout<<endl<<"Pivot for how many seconds?  (one rev ~15)"<<endl;
    cin>>desired;
}while(desired<0||desired>15);

    poke();
  //drive(0,40,00,001);//pivot left nice and slow
    drive(0,40,255,255);//pivot right nice and slow
    gpioSleep(PI_TIME_RELATIVE,desired,0000);
    stop();
    gpioSleep(PI_TIME_RELATIVE,1,0);
	cout<<"New Heading After Stop = "<<mySensors.polarHeading()<<endl;
}
void driveFwd(){
//void driveFwd(int velHighByte, int velLowByte, int radHighByte, int radLowByte){
int desired=-1;
do{
    cout<<endl<<"5 cm/sec for how many seconds?  (20 max)"<<endl;
    cin>>desired;
}while(desired<0||desired>20);
serWriteByte(USBHandle,131); //safe mode while under way
char driveString []= {137,0,50,127,255};//137 is drive initializer. 127/255 = straight;
serWrite(USBHandle,driveString,5);
    gpioSleep(PI_TIME_RELATIVE,desired,0000);
    stop();
    gpioSleep(PI_TIME_RELATIVE,1,0);

}

void spin()
{
    poke();
    drive(0,30,255,255);//pivot right nice and slow
}

void driveSlowRev(){//120mm/sec reverse
drive(255,136,0,0);
}

void drive(int velHighByte, int velLowByte, int radHighByte, int radLowByte){
    serWriteByte(USBHandle,131); //safe mode while under way***bad for undocking***
char driveString []= {137,velHighByte,velLowByte,radHighByte,radLowByte};//137 is drive initializer
serWrite(USBHandle,driveString,5);
}

void shutdownSequence(){
cout << "shutdown Sequence";
    serWriteByte(USBHandle,133); //force roomba sleep
    gpioSleep(PI_TIME_RELATIVE,0,100000);
    serClose(USBHandle);
    gpioTerminate();
}

void poke(){ //used to make sure roomba awake and listening
 int R1 = 23;
    gpioSetMode(23, PI_OUTPUT);
    gpioWrite(R1, 0); //pulse wake relay
    gpioSleep(PI_TIME_RELATIVE,0,110000); //should be 50 milliseconds
    gpioWrite(R1, 1); //this is NOT and LED, stupid. Leave it on when not pulsing to wake
    cout << "Poked" << endl;
    serWriteByte(USBHandle,128); //start command/passive mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);
    serWriteByte(USBHandle,131); //safe mode
    gpioSleep(PI_TIME_RELATIVE,0,150000);
}

int pollBatt(){
int volts=controlVoltage;
read();
volts =controlVoltage;

return volts;
}

