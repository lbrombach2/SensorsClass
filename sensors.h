#ifndef SENSORS_H
#define SENSORS_H

#include <iostream>
#include <pigpio.h>
#include "sensors.h"

using namespace std;

const int laserRelay = 17;
const int rx =18;//defining bitbang pin for Lidar
class sensors
{
    public:
      // sensors(); //constructor
       //virtual ~sensors(); //destructor
	/////////////////for compass and accelerometer (LSM303)//////////////////////
	int I2C_Accel();//returns 1 if Z axis close to 1 G (device =level-ish)
	void I2C_Comp_Init(); //call once in main()
	int I2C_Comp_Read();
	int polarHeading(); //calls comp_read() and converts to polar heading
	int I2C_Comp_Calibrate();
	bool I2C_Comp_Zero();// adjusts heading to make current direction = 0. Makesure calibrated first.
        int I2C_Comp_Start();
	int I2C_Comp_Stop();


	///////////////////for lidar//////////////////////////////////////////

        int scan();//spools up lidar and grabs raw data for processing
        int getDis(int bearing); //returns data from last scan(). Don't forget 0 is at my right and 90 is straight ahead


    protected:

    private:
/////////////////for compass and accelerometer (LSM303)//////////////////////


///////////////////for lidar//////////////////////////////////////////
	int checksumPacket[10]={0};
	int cycles =0; //how many loops reading lidar_Read() have gone by during call

    	bool legit(int packet[]); //tests checksum
	bool full(); //returns true if > a defined percentage of 360 points have been acquired. usually called by lidar_Read()

};



#endif // SENSORS_H
