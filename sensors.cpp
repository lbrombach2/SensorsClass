/*
This is to hopefully conglomerate all non roomba sensor functions. Keep devices together. List them here
In order:
LSM303
XV11 Lidar
2nd from last are twos complement conversion functions
constructor and destructor are at the bottom
*/

#include "sensors.h"
#include<cstdlib>
#include<math.h>
#include<fstream>


using namespace std;

sensors mySensors;

const double PI=3.141592;
int LSM303_compass=0x1E; //adafruit flora compass address
int LSM303_accel=0x19;  //adafruit flora accelerometer address
int I2C_Handle=-1;
int I2Cbus=1; //(RPiC stndard issue)
int magXOffset=0;
int magYOffset=0;
int magZOffset=0;
int magX=0;
int magY=0;
int magZ=0;
char bufC[6];
char bufA[6];
int dataArr[361][4]={0};//dataArr[360][0] shall contain heading when scan taken
static inline int SignedToInt(uint8_t low, int8_t high);

struct lastCal{
int lastXOffset;
int lastYOffset;
int lastZOffset;
time_t lastCalibration;
double zeroAdjustment;//for aligning heading output to desired
};
lastCal lastCal;//create the struct object;

////////////////////////START ACCELEROMETER READ//////////////////////
int sensors::I2C_Accel(){
int accelX=0;
int accelY=0;
int accelZ=0;
int I2C_Accel_Handle=i2cOpen(I2Cbus, LSM303_accel,0);
if (I2C_Accel_Handle>=0)cout<<"Good to go so far"<<endl;
else cout<<"NOOOOPE, NO I2C HERE"<<endl;

if(i2cWriteByteData(I2C_Accel_Handle, 0x20, 0x47)==0) //set frequencyset frequency
//if(i2cWriteByteData(I2C_Accel_Handle, 0x20, 0x27)==0) //Data Rate: 10Hz, disable low-power, enable all axes
cout<<"frequency command accepted"<<endl;
else cout<<"freq command not accepted"<<endl;
gpioDelay(1000);
//if(i2cWriteByteData(I2C_Accel_Handle, 0x23, 0x49)==0) //continuous update, LSB at lower addr, +- 2g, Hi-Res disable
if(i2cWriteByteData(I2C_Accel_Handle, 0x23, 0x09)==0) //continuous update, LSB at lower addr, +- 2g, Hi-Res disable
cout<< "mode command accepted"<<endl;
else cout<<"mode command not accepted"<<endl;
gpioDelay(100000);

int xLSB=0;
int xMSB=0;
int yLSB=0;
int yMSB=0;
int zLSB=0;
int zMSB=0;
for(int i=0;i<3;i++){
bufA[0] = i2cReadByteData(I2C_Accel_Handle, 0x28); //accel X LSB
bufA[1] = i2cReadByteData(I2C_Accel_Handle, 0x29); //accel X MSB
bufA[2] = i2cReadByteData(I2C_Accel_Handle, 0x2A); //accel Y LSB
bufA[3] = i2cReadByteData(I2C_Accel_Handle, 0x2B); //accel Y MSB
bufA[4] = i2cReadByteData(I2C_Accel_Handle, 0x2C); //accel Z LSB
bufA[5] = i2cReadByteData(I2C_Accel_Handle, 0x2D); //accel Z MSB


//for(int i=0;i<6;i++)
//cout<<+bufA[i]<<"   ";
//cout<<endl;

if(bufA[1]>=128){
int temp=(((+bufA[0])+(+bufA[1]<<8))>>4);
xLSB=temp%256;
xMSB=(((temp/256)*256)>>8)+240;
}
else {
int temp=(((+bufA[0])+(+bufA[1]<<8))>>4);
xLSB=temp%256;
xMSB=((temp/256)*256)>>8;
}


if(bufA[3]>=128){
int temp=(((+bufA[2])+(+bufA[3]<<8))>>4);
yLSB=temp%256;
yMSB=(((temp/256)*256)>>8)+240;
}
else {
int temp=(((+bufA[2])+(+bufA[3]<<8))>>4);
yLSB=temp%256;
yMSB=((temp/256)*256)>>8;
}

if(bufA[5]>=128){
int temp=((+bufA[4]>>4)+(+bufA[5]<<4));
//int temp=1050;
zLSB=temp%256;
zMSB=(((temp/256)*256)>>8)+240;
}
else {
int temp=((+bufA[4]>>4)+(+bufA[5]<<4));
//int temp=1050;
zLSB=temp%256;
zMSB=((temp/256)*256)>>8;
}
accelX=SignedToInt(xLSB, xMSB); //must send low byte (LSB)first
accelY=SignedToInt(yLSB, yMSB); //must send low byte (LSB)first
accelZ=SignedToInt(zLSB, zMSB); //must send low byte (LSB)first

cout<<endl<<accelX<<"   "<<accelY<<"   "<<accelZ<<endl;


gpioSleep(PI_TIME_RELATIVE,0,300000);
}

if(i2cWriteByteData(I2C_Accel_Handle, 0x20, 0x00)==0);//puts accelerometer into sleep mode
if(i2cClose(I2C_Accel_Handle)==0)cout<<"Closed up OK too"<<endl;

if(accelZ>1000 && accelX<100 && accelY<100) return 1;
else return 0;
}
/////////////////START COMPASS INIT()///////////////////////////
void sensors::I2C_Comp_Init(){
ifstream getLast;
getLast.open("lastCal.txt");
if(getLast){
getLast>>lastCal.lastXOffset;
getLast.ignore();
getLast>>lastCal.lastYOffset;
getLast.ignore();
getLast>>lastCal.lastZOffset;
getLast.ignore();
getLast>>lastCal.lastCalibration;
getLast.ignore();
getLast>>lastCal.zeroAdjustment;
getLast.close();

cout<< "Calibration data retrieved. x,y offset, zeroadustment are: "
	<<lastCal.lastXOffset<<", "<<lastCal.lastYOffset<<" ... "<<lastCal.zeroAdjustment<<endl;
cout<< "Last calibration was "<<(time(NULL)-lastCal.lastCalibration)/60 <<" minutes ago."<<endl<<endl;
}else cout<<"No file here, please calibrate"<<endl;

if(time(NULL)-lastCal.lastCalibration>86400){
cout<<"############ MAGNETOMETER CALIBRATION OVERDUE, PLEASE CALIBRATE##################"<<endl;
}

magXOffset=lastCal.lastXOffset;
magYOffset=lastCal.lastYOffset;
magZOffset=lastCal.lastZOffset;


}
//////////////////START COMPASS ZERO()//////////////////////////
bool sensors::I2C_Comp_Zero(){
if(time(NULL)-lastCal.lastCalibration>86400){
	cout<<"ZEROING REQUIRES MORE RECENT CALIBRATION"<<endl;
	return false;  //false meanse calibration failed
	}
lastCal.zeroAdjustment=0;
int zero =I2C_Comp_Read();
//zero+=lastCal.zeroAdjustment;// (must undo zero adjustment done in read())
//if(zero>359){zero-=360;}
//lastCal.zeroAdjustment=0;
cout<<"zeroing. comp_read = "<<zero<<endl;

gpioDelay(5000);
zero+=I2C_Comp_Read();
gpioDelay(5000);
zero+=I2C_Comp_Read();
zero=zero/3;
cout<<"new zero = "<<zero<<endl;
lastCal.zeroAdjustment=zero;

ofstream setLast;
setLast.open("lastCal.txt");
setLast<<magXOffset<<'\n'<<magYOffset<<'\n'<<magZOffset<<'\n'<<time(NULL)<<'\n'<<lastCal.zeroAdjustment<<'\n';
setLast.close();
return true;
}
/////////////////START COMPASS START()//////////////////////////

int sensors::I2C_Comp_Start(){


I2C_Handle=i2cOpen(I2Cbus, LSM303_compass,0);
if (I2C_Handle<0)cout<<"UNABLE TO OPEN I2C"<<endl;

if(i2cWriteByteData(I2C_Handle, 0x00, 0xF8)!=0) //set frequency
cout<<"freq command to compass not accepted!!!!"<<endl;
gpioDelay(5000);
if(i2cWriteByteData(I2C_Handle, 0x02, 0x00)!=0) //set mode - 0 for continuous, 1 for single, 3 for off
cout<<"mode command not accepted"<<endl;
return 0;
}
////////////////////////START COMPASS READ()///////////////////
int sensors::I2C_Comp_Read(){
double heading=0;
double headingDeg;

bufC[0] = i2cReadByteData(I2C_Handle, 0x03);//x MSB
bufC[1] = i2cReadByteData(I2C_Handle, 0x04);//x LSB
bufC[4] = i2cReadByteData(I2C_Handle, 0x05);//Y MSB  **NOT a typo, Z comes out first
bufC[5] = i2cReadByteData(I2C_Handle, 0x06);//Y LSB
bufC[2] = i2cReadByteData(I2C_Handle, 0x07);//Z MSB ** NOT a typo, Y comes out sencond
bufC[3] = i2cReadByteData(I2C_Handle, 0x08);//Z LSB

//for(int i=0;i<6;i++)
//cout<<+bufC[i]<<"   ";
//cout<<endl;

magX=(SignedToInt(+bufC[1], +bufC[0]))-magXOffset; //must send low byte (LSB)first
magY=(SignedToInt(+bufC[3], +bufC[2]))-magYOffset; //must send low byte (LSB)first
magZ=(SignedToInt(+bufC[5], +bufC[4]))-magZOffset; //must send low byte (LSB)first

//calculate heading and set so facing dock = north (0 degrees - in cartesian coordinates)
heading=atan2(magY, magX);
headingDeg=heading*180/PI;
if(headingDeg<0)headingDeg+=360;//correct to keep output > 0
//cout<<"headingDegRaw - zeroadjust.."<<headingDeg<<" - " <<lastCal.zeroAdjustment<<"  ";
headingDeg-=lastCal.zeroAdjustment;
if(headingDeg<0)headingDeg+=360;

gpioDelay(1000);

return headingDeg;
}
///////////////////START COMPASS POLAR READ()///////////////////
int sensors::polarHeading(){//converts cartesian heading from read() to polar heading (ie unit circle)
int polar=450-mySensors.I2C_Comp_Read();
polar = (polar > 359) ? polar-360:polar;
return polar;
}

//////////////////////START COMPASS STOP()////////////////////////(power down)
int sensors::I2C_Comp_Stop(){
i2cWriteByteData(I2C_Handle, 0x02, 0x03); //set mode - 0 for continuous, 1 for single, 3 for off
if(i2cClose(I2C_Handle)==0);//cout<<"Closed up OK too"<<endl;
return 0;
}
///////////////////////////////////START COMPASS CALIBRATE//////////////////////////////////
int sensors::I2C_Comp_Calibrate(){

int maxX=0;
int maxY=0;
int maxZ=0;

int minX=0;
int minY=0;
int minZ=0;

I2C_Comp_Start();
cout<<"Calibration in progress. Stand by";
for(int i=0;i<2500;i++){
bufC[0] = i2cReadByteData(I2C_Handle, 0x03);//x MSB
bufC[1] = i2cReadByteData(I2C_Handle, 0x04);//x LSB
bufC[4] = i2cReadByteData(I2C_Handle, 0x05);//Y MSB  **NOT a typo, Z comes out first
bufC[5] = i2cReadByteData(I2C_Handle, 0x06);//Y LSB
bufC[2] = i2cReadByteData(I2C_Handle, 0x07);//Z MSB ** NOT a typo, Y comes out sencond
bufC[3] = i2cReadByteData(I2C_Handle, 0x08);//Z LSB

//for(int i=0;i<6;i++)
//cout<<+bufC[i]<<"   ";
//cout<<endl;

magX=SignedToInt(+bufC[1], +bufC[0]); //must send low byte (LSB)first
magY=SignedToInt(+bufC[3], +bufC[2]); //must send low byte (LSB)first
magZ=SignedToInt(+bufC[5], +bufC[4]); //must send low byte (LSB)first

//cout<<magX<<"   "<<magY<<"   "<<magZ<<endl;

if(magX<minX||minX==0){minX=magX;}// cout<<"####################MINX###################"<<endl;}
if(magX>maxX||maxX==0){maxX=magX;}// cout<<"####################MAXX###################"<<endl;}
if(magY<minY||minY==0){minY=magY;}// cout<<"####################MINY###################"<<endl;}
if(magY>maxY||maxY==0){maxY=magY;}// cout<<"####################MAXY###################"<<endl;}
if(magZ<minZ||minZ==0){minZ=magZ;}// cout<<"####################MINZ###################"<<endl;}
if(magZ>maxZ||maxZ==0){maxZ=magZ;}// cout<<"####################MAXZ###################"<<endl;}
gpioDelay(10000);
}
cout << endl;
I2C_Comp_Stop();
//if(i2cClose(I2C_Handle)==0)cout<<"Closed up OK too"<<endl;
cout<<"min/max X "<<minX<<" "<<maxX;
cout<<"  min/max Y "<<minY<<" "<<maxY;
cout<<"  min/max Z "<<minZ<<" "<<maxZ;


magXOffset=(minX+maxX)/2;
magYOffset=(minY+maxY)/2;
magZOffset=(minZ+maxZ)/2;

ofstream setLast;
setLast.open("lastCal.txt");
setLast<<magXOffset<<'\n'<<magYOffset<<'\n'<<magZOffset<<'\n'<<time(NULL)<<'\n'<<lastCal.zeroAdjustment<<'\n';
setLast.close();

return 0;
}
////////////////////////////////////END COMPASS CALIBRATE///////////////////////////////////
////////////////////////////////////END LSM303 SECTION//////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

int sensors::scan(){
    cycles=0;
    int startTime=time(NULL);
	const int bufSize=2000; //used for lidar bitbang in


    for (int i=0;i<360;i++){ //for each read, shift previous reading up the array from 0-2
        dataArr[i][2]=dataArr[i][1];
        dataArr[i][1]=dataArr[i][0];
        dataArr[i][0]=0; //reset 0 back to 0 each scan
	if(i==0)dataArr[i][3]=0;} //reset 3 back to 0 each new scan
    dataArr[360][0]=mySensors.I2C_Comp_Read();//set to current heading each scan
    gpioWrite(laserRelay,0); //0=lidar powered and on
    gpioSleep(PI_TIME_RELATIVE,3,2000); //time for lidar to spin up

while(full()==false&&cycles<20){

    cycles++;
	gpioSleep(PI_TIME_RELATIVE,0,100000);  //10ms delay
    if(gpioSerialReadOpen(rx,115200,8)!=0){cout<<"unable to open bitbang"<<endl;
    }else{ //	cout<<"bitbang open ok"<<endl;
        char buf[bufSize];
	for(int i=0; i < bufSize; i++) buf[i]=0;
        gpioSleep(PI_TIME_RELATIVE,1,10);  //10ms delay
// 	cout<<"reading this many int buf[] - "<<gpioSerialRead(rx, buf, bufSize)<<endl;
	gpioSerialRead(rx, buf, bufSize);
        gpioSerialReadClose(rx);

    for(int i=0; i < bufSize; i++){ //identify packet starts and send to checksum
        if(buf[i]==250){
            int packet[22];
            for (int j=0;j<22;j++){
            packet[j]=static_cast<unsigned int>(buf[i+j]);//comes out as char, need to send int
            }

            if(legit(packet)){//tests checksum and adds to dataArr[] if legit
        //stick(packet);//add packet to matrix to compare to other samples and determine best?
            }
        }
    }
}

        }

    gpioWrite(laserRelay,1); //1 = lidar relay off


    for (int i=0;i<360;i++){
     cout<<"Heading "<<i<<"  dis "<<dataArr[i][0]<<   "   str "<<dataArr[i][3]<<endl;
     }
cout<<"##########TOOK "<<time(NULL)-startTime<<" seconds########"<<endl;
    return 0;
}

bool sensors::legit(int packet[]){

int txDchksum = (packet[20] + (packet[21] << 8));//combine checksum bytes to get transmitted checksum

for(int i=0;i<10;i++){
checksumPacket[i]=(packet[(2*i)]+ (packet[2*i+1]<<8)); //calculate checksum on rest of packet
}
int chk32=0;
for(int i=0;i<10;i++){
chk32=(chk32<<1)+checksumPacket[i];
}
    int checksum;
    checksum = (chk32 & 0x7FFF) + ( chk32 >> 15 ); // wrap around to fit into 15 bits
    checksum = checksum & 0x7FFF; // truncate to 15 bits




	if (checksum==txDchksum && txDchksum!=0){
		int theta=((packet[1]-160)*4)+90;
		if (theta>359)theta-=360;
    if(theta<0){return false;}
		int speed = checksumPacket[1];
		int d1Dis = checksumPacket[2];
		int d1Str = checksumPacket[3];
		int d1Theta = theta;
		int d2Dis = checksumPacket[4];
		int d2Str = checksumPacket[5];
		int d2Theta = theta+1;
		int d3Dis = checksumPacket[6];
		int d3Str = checksumPacket[7];
		int d3Theta = theta+2;
		int d4Dis = checksumPacket[8];
		int d4Str = checksumPacket[9];
		int d4Theta = theta+3;
	    if(d1Str>dataArr[d1Theta][3]){ //if signal strength(on data[i][3]) stronger than last, add here
		dataArr[d1Theta][0]=d1Dis; //add extra error detecting/averaging here too?
		dataArr[d1Theta][3]=d1Str;
	    }
	    if(d2Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d2Theta][0]=d2Dis;
		dataArr[d2Theta][3]=d2Str;
	    }
	    if(d3Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d3Theta][0]=d3Dis;
		dataArr[d3Theta][3]=d3Str;
	    }
	    if(d4Theta==360){d4Theta=0;}
	    if(d4Str>dataArr[d4Theta][3]){ //add extra error detecting/averaging here?
		dataArr[d4Theta][0]=d4Dis;
		dataArr[d4Theta][3]=d4Str;
		}

return true;
}
else return false;
}

bool sensors::full(){ //checks to see how many points out of 360 we've acquired
int tally=0;
 int percent =0;
    for (int i=0;i<360;i++){
	if (dataArr[i][0]!=0) tally++;
    }
   percent =(tally*100)/360;
    cout<<"Starting read cycle "<<cycles+1<<"    ";
    cout<<percent<< "% of points acquired"<<endl;
if(percent>70) return true;  //Through some trial and error, I picked 65% for a "best return on time waited" Still testing
else return false;
}

int sensors::getDis(int bearing){
if (dataArr[bearing][3]>20){
return dataArr[bearing][0];
}

return 0;
}





//////////////////////////////////END XV11 LIDAR SECTION/////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// Helper function for converting 1 or 2 chars into int (unsigned).
static inline int UnsignedToInt(uint8_t low, uint8_t high=0)
{
    return (static_cast<int>(high) << 8) + static_cast<int>(low);
}

// Helper function for converting 2 chars into int (signed).
static inline int SignedToInt(uint8_t low, int8_t high)
{
    return (static_cast<int>(high) << 8) + static_cast<int>(low);
}
//////////////////////////////END CONVERSION HELPER FUNCTIONS//////////////////////////////

//sensors::sensors()   //constructor
//{
    //ctor
//}

//sensors::~sensors()  //destructor
//{
    //dtor
//}
