#include <iostream>
#include <pigpio.h>

using namespace std;

#include <iostream>
#include <stdio.h>
#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <errno.h>
#include <pigpio.h>
#include <time.h>
#include "roombaSerial.h"
#include "sensors.h"

int request=-1;

using namespace std;


int main()
{



    if(gpioInitialise() < 0){
	cout << "GPIO initializing failed. Try again asshole." << endl;
	gpioSleep(PI_TIME_RELATIVE,0,1500000);
	return -1;
	}

    mySensors.I2C_Comp_Init();
    roombaWake();
    gpioSetMode(laserRelay, PI_OUTPUT);
    gpioWrite(laserRelay, 1); //start with laser off
    gpioDelay(50000);
       int x=0;
    time_t seconds = time(NULL);
    time_t timeDocked = time(NULL);


cout<< "Starting I2C Testing Mode. Voltage = " << pollBatt() << endl;
system("raspistill -o lastsnap -p '0,40,400,400' -t 3000&");


do{
if(pollBatt()<13800)
cout<<"WARNING WARNING WARNING - VOLTAGE "<<pollBatt()<<" - PLEASE EXIT AND PUT ME IN MAINTENANCE MODE "<<endl;

mySensors.I2C_Comp_Start();//actually move out of loop once calibrate is changed to not stop compass

cout<<endl<<" 1 to dock\n 2 to undock\n 3 to face dock\n 4 to turn to heading\n 6 to read Compass\n"
<<" 7 to Calibrate Compass\n 8 to read Accelerometer\n 9 to check voltage\n 0 to quit and exit"<<endl;
cout<<"111 to drive Forward, 222 to pivot right\n"
<<"333 to get Get distance to bearing, 444 for new Lidar Scan\n"
<<"555 laser on, 666 laser off"<<endl;
cin>>request;

if(request==1)dock();
if(request==2){undock();gpioSleep(PI_TIME_RELATIVE,1,100000);mySensors.I2C_Comp_Zero();}
if(request==3)faceDock(); //spins at least once then face dock
if(request==4)faceHeading(); //seeks input heading for 30 seconds
if(request==6){cout<<"Cartesian Heading = "<<mySensors.I2C_Comp_Read()
		<<"   Polar Heading = "<<mySensors.polarHeading()<<endl;}
if(request==7){spin();mySensors.I2C_Comp_Calibrate();stop();}
//if(request==7){I2C_Comp_Calibrate();}
if(request==8)mySensors.I2C_Accel();
if(request==9)cout<<"voltage = "<<pollBatt()<<endl;
if(request==111)driveFwd();
if(request==222)pivot();
if(request==333){
    int in=-1;
    while(in<0||in>359){
    cout<<endl<<"0-359?"<<endl;
    cin>>in; cout<<endl;
    }
    cout<<mySensors.getDis(in)<<endl;
    }
if(request==444){mySensors.scan();}
if(request==555){gpioWrite(laserRelay, 0);} //laser on
if(request==666){gpioWrite(laserRelay, 1);} //laser off


if (request!=0)request=-1;

}while(request!=0);
gpioWrite(laserRelay, 1); //make sure laser off
mySensors.I2C_Comp_Stop();



    cout << "done with do loop, exiting"<<endl;
    gpioSleep(PI_TIME_RELATIVE,1,0);


    shutdownSequence();//includes gpioterminate(). leave  forvery last
    cout << "Exiting" ;
    return 0;
}


/*
int main()
{
gpioinit....
i2c comp(lsm303)init



    sensors sensors;  //create the object
    gpioSetMode(sensors.lidarOn, PI_OUTPUT);
    gpioSetMode(sensors.rx, PI_INPUT);




//destroy object, run destructor?
i2c terminate
compass stop
laserrelay off
   gpiterminate........
    return 0;
}*/
