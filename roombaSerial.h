#ifndef ROOMBASERIAL_H_INCLUDED
#define ROOMBASERIAL_H_INCLUDED

#include "sensors.h"

extern sensors mySensors;


extern int USBHandle;
extern int controlVoltage;
int* read();
void shutdownSequence();
int pollBatt();
void spin();
void pivot();
void driveFwd();
void driveSlowRev();
void stop();
int *parse(int packet[]);

void poke();  //pulses relay and issues passive -> safe mode commands to ensure roomba listening
void roombaWake(); //initializes once per program
void roombaSleep();
void startSensors();
void parseData();
void sampleData();
void undock();
void dock();
void drive(int velHighByte, int velLowByte, int radHighByte, int radLowByte);
void driveSlowRev();
void faceDock();
void faceHeading();


#endif // ROOMBASERIAL_H_INCLUDED
